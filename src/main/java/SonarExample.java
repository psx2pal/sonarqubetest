public class SonarExample {

    public static final String MY_VAR = "my_var";

    public static void main(String[] args) {

    }


    public SonarExample() {
        String test = "test";
    }

    public void myEmptyMethod() {

    }

    /*
    dies ist ein sehr langer kommentar
    dies ist ein sehr langer kommentar
    dies ist ein sehr langer kommentardies ist ein sehr langer kommentar
    dies ist ein sehr langer kommentar
    dies ist ein sehr langer kommentar
    dies ist ein sehr langer kommentar
     */
    public String myException() throws Exception {
        String myString = getMyString();
        if(myString.length() > 5) {
            System.out.println("Bigger than 5");
        }

        if(myString.length() > 5) {
            if (true) {
                int i = 0;
                while(true) {
                    i++;

                    if(i == 10)
                        break;

                    if("test".equals("test")) {
                        if (1==1) {

                        }
                    }
                }
            }
        }

        String test = "";

        if(test.isEmpty()) {
            try {
                throw new Exception("Haha my ultimate Exception");
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Haha my new ultimate Exception");
            }
        }

        // TODO do something

        return test;
    }

    public void newMethod() {
        System.out.println("my_var");
        System.out.println("my_var2");
        System.out.println("my_var2");
        System.out.println("my_var2");
        System.out.println("my_var2");
    }

    public String getMyString() {
        return null;
    }


    public void test1(String var1, String var2) {
        String test = "test";
        String var1_1 = var1;
        String var2_1 = var2;

        if(var1_1.equals(var2_1)) {
            String var4 = "var4";
        }
    }
    public void test1(String var1, String var2, String var3) {
        String test = "test";
        String var1_1 = var1;
        String var2_1 = var2;

        if(var1_1.equals(var2_1)) {
            String var4 = "var4";
        }

        String var3_1 = var3;
    }

//    public void test1(String var1, String var2, String var3) {
//        String test = "test";
//        String var1_1 = var1;
//        String var2_1 = var2;
//
//        if(var1_1.equals(var2_1)) {
//            String var4 = "var4";
//        }
//
//        String var3_1 = var3;
//    }

    public static void duplication1(String var1, String var2, String var3) {
        String test = "test";
        String var1_1 = var1;
        String var2_1 = var2;

        if(var1_1.equals(var2_1)) {
            String var4 = "var4";
        }

        String var3_1 = var3;
    }
}
