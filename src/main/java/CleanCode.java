// https://eckcellent-it.blog/clean-code-part-1/
// https://eckcellent-it.blog/clean-code-part-2/

// https://jaxenter.de/das-fuenfsekundenexperiment-guter-code-schlechter-code-33196

import java.util.ArrayList;
import java.util.List;

public class CleanCode {

// ## Unstrukturierter Code ##
//    public List findMaleAdultCustomers() {
//        List custFound = new ArrayList();
//        for (int i = 0; i < custList.size(); i++) {
//            if (custList.get(i).getAge() < 18)
//                continue;
//            else
//            {
//                if (custList.get(i).getGender() != Gender.FEMALE)
//                    custFound.add(custList.get(i)); }
//        }
//        return custFound;
//    }

//    public List findMaleAdultCustomers() {
//        List maleAdultCustomers = new ArrayList<>();
//        for (Customer customer : customers) {
//            if (isMale(customer) && isAdult(customer)) {
//                maleAdultCustomers.add(customer);
//            }
//        }
//        return maleAdultCustomers;
//    }
//    private boolean isMale(Customer customer) {
//        return customer.getGender() == Gender.MALE;
//    }
//    private boolean isAdult(Customer customer) {
//        return customer.getAge() >= 18;
//    }




//##    Unnötiger Code ##
//    public boolean containsCustomer(final Customer argCustomer) {
//        if (customers.contains(argCustomer) == true) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//
//    public boolean containsCustomer(final Customer argCustomer) {
//        return customers.contains(argCustomer);
//    }

}
